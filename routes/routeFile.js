var express = require('express');
var router = express.Router();
var User = require('../models/users');
var fs = require('fs');
var crypto = require('crypto');
var ALGORITHM = process.env.ALGORITHM;
var SIGNATURE_FORMAT = process.env.SIGNATURE_FORMAT; 


//POST route for creating user
router.post('/setUser', function (req, res, next) {
    if (req.body.username && req.body.password) {
        var userData = {
            username: req.body.username,
            password: req.body.password
        }
        User.create(userData, function (error, user) {
            if (error) {
                return next(error);
            } else {
                return res.send('User Saved');
            }
        });
    } else {
        var err = new Error('missing required fields.');
        err.status = 400;
        return next(err);
    }
});

//POST route for adding publicKey to user
router.post('/addPubKey', function (req, res, next) {
    console.log(req.body);
    if (req.body.username && req.body.password && req.body.publicKey) {
        User.authenticate(req.body.username, req.body.password, function (error, user) {
            if (error || !user) {
                var err = new Error('Wrong username or password.');
                err.status = 401;
                return next(err);
            } else {
                var query = {username: req.body.username};
                try {
                    fs.writeFileSync('./keys/' + user._id + "_pubKey.pem", req.body.publicKey);
                    return res.send('User '+user.username+' has added a public key.');
                } catch(e) {
                    return res.send(e);
                }
            }
        });
    } else {
        var err = new Error('All fields required.');
        err.status = 400;
        return next(err);
    }
});


//POST route for verifying message signature against users publicKey
router.post('/verifySignature', function (req, res, next) {
    if (req.body.username && req.body.password && req.body.message&& req.body.signature) {
        User.authenticate(req.body.username, req.body.password, function (error, userData) {
            if (error || !userData) {
                var err = new Error('Wrong username or password.');
                err.status = 401;
                return next(err);
            } else {
                try {
                    var publicKey = fs.readFileSync('./keys/' +userData._id + '_pubKey.pem', 'utf8');
                } catch(e) {
                    return res.send('No key file exists for this user.');
                }
                var verify = crypto.createVerify(ALGORITHM);
                verify.update(req.body.message);
                var verification = verify.verify(publicKey, req.body.signature, SIGNATURE_FORMAT);
                return res.send('Verification result: ' + verification.toString().toUpperCase());
            }
        });
    } else {
        var err = new Error('All fields required.');
        err.status = 400;
        return next(err);
    }
});

//POST route for verifying message signature against default publicKey
router.post('/useDefaultKeys', function (req, res, next) {
    console.log(req.body);
    if (req.body.username && req.body.password && req.body.message && req.body.signature) {
        User.authenticate(req.body.username, req.body.password, function (error, userData) {
            if (error || !userData) {
                var err = new Error('Wrong username or password.');
                err.status = 401;
                return next(err);
            } else {
                try {
                    var publicKey = fs.readFileSync('./keys/richard2018_pubKey.pem', 'utf8');
                } catch(e) {
                    return res.send('No default key file exists for this server.');
                }
                var verify = crypto.createVerify(ALGORITHM);
                verify.update(req.body.message);
                var verification = verify.verify(publicKey, req.body.signature, SIGNATURE_FORMAT);
                console.log(verification);
                return res.send('Verification result: ' + verification.toString().toUpperCase());
            }
        });
    } else {
        var err = new Error('All fields required.');
        err.status = 400;
        return next(err);
    }
});

module.exports = router;