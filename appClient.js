var crypto = require('crypto');
var fs = require('fs');
var bcrypt = require('bcrypt');
var request = require('request');
var ALGORITHM = process.env.ALGORITHM;
var SIGNATURE_FORMAT = process.env.SIGNATURE_FORMAT;

//determine call to make by task submitted in CLI
var task = process.argv[2];
console.log('You are callling %s on the appServer. ', task);
switch(task) {
    case 'setUser':
        if(process.argv.length == 5) {
            setUser(process.argv[3], process.argv[4]);
        } else {console.log('Incorrect number of arguments for task setUser.');}
        break;
    case 'addPubKey':
        if(process.argv.length == 6) {
            addPubKey(process.argv[3], process.argv[4], process.argv[5]);
        } else {console.log('Incorrect number of arguments for task. addPubKey');}
        break;
    case 'verifySignature':
        if(process.argv.length == 7) {
            verifySignature(process.argv[3], process.argv[4], process.argv[5], process.argv[6]);
        } else {console.log('Incorrect number of arguments for task verifySignature.');}
        break;
    case 'useDefaultKeys':
        if(process.argv.length == 6) {
            useDefaultKeys(process.argv[3], process.argv[4], process.argv[5]);
        } else {console.log('Incorrect number of arguments for task useDefaultKeys.');}
        break;
    default:
        console.log('Invalid task submitted');
        break;
};

//adds a user/password pair to the mongoDB, no duplication of usernames allowed
function setUser(username, password){
    request.post(process.env.SERVERADDR + '/setUser', {
        json: {
            username: username,
            password: password
        }
    }, function(error, result){
        if (!error){
            console.log('Task result: ', result.body);
        } else {
            console.log('setUser ERROR: ', error)
        }
    })
};

//will add a public key .pem file on the server if user authenticates
function addPubKey(username, password, key){
    request.post(process.env.SERVERADDR + '/addPubKey', {
        json: {
            username: username,
            password: password,
            publicKey: key
        }
    }, function(error, result){
        if (!error){
            console.log('Task result: ', result.body);
        } else {
            console.log('addPubKey ERROR: ', error)
        }
    })
};

//will verify any message/signature set against the users public key on the server if user authenticates
function verifySignature(username, password, message, signature){
    request.post(process.env.SERVERADDR + '/verifySignature', {
        json: {
            username: username,
            password: password,
            message: message,
            signature: signature
        }
    }, function(error, result){
        if (!error){
            console.log('Task result: ', result.body);
        } else {
            console.log('verifySignature ERROR: ', error)
        }
    })
};

//will use the dafault key set in client and server to verify any message submitted if user authenticates
function useDefaultKeys(username, password, message){
    var privateKey = fs.readFileSync('./keys/richard2018_privKey.pem', 'utf8');
    var sign = crypto.createSign(ALGORITHM);
    sign.update(message);
    var signature = sign.sign(privateKey, SIGNATURE_FORMAT);

    request.post(process.env.SERVERADDR + '/useDefaultKeys', {
        json: {
            username: username,
            password: password,
            message: message,
            signature: signature
        }
    }, function(error, result){
        if (!error){
            console.log('Task result: ', result.body);
        } else {
            console.log('useDefaultKeys ERROR: ', error)
        }
    })
};