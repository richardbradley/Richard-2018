var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

var UserSchema = new mongoose.Schema({
  username: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  password: {
    type: String,
    required: true,
  }
});

//authenticate input against database
UserSchema.statics.authenticate = function (username, password, callback) {
  User.findOne({ username: username })
    .exec(function (err, user) {
      if (err) {
        return callback(err)
      } else if (!user) {
        var err = new Error('User not found.');
        err.status = 401;
        return callback(err);
      }
      bcrypt.compare(password, user.password, function (err, result) {
        if (result === true) {
          return callback(null, user);
        } else {
          return callback();
        }
      })
    });
}

//hashing all passwords before saving, no duplicate users
UserSchema.pre('save', function (next) {
    var user1 = this;
    User.findOne({ username: user1.username })
    .exec(function (err, user) {
      if (err) {
        return callback(err)
      } else if (user) {
        var err = new Error('User already exists.');
        err.status = 423;
        return next(err);
      }
      bcrypt.hash(user1.password, parseInt(process.env.SALT), function (err1, hash) {
        if (err1) {
          return next(err1);
        }
        user1.password = hash;
        next();
      });
    });
});

var User = mongoose.model('User', UserSchema);
module.exports = User;